import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuicklinkModule } from 'ngx-quicklink';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    QuicklinkModule,
    HttpClientModule
  ],
  exports: [
    QuicklinkModule
  ]
})
export class SharedModule { }
